import ballerina/http;
import ballerina/io;
import ballerinax/kafka; 
import ballerina/log;

kafka:ConsumerConfiguration consumerConfigs = {
    groupId: "test-consumer-group",
    topics: ["dsar"],
    pollingInterval: 1,
    autoCommit: false

};
kafka:Consumer kafkaConsumer=check new(kafka:DEFAULT_URL,consumerConfigs);
kafka:TopicPartition topicPartition = {
    topic:"dsar",
    partition: 0

};
listener kafka:Listener kafkaListener =
        new (kafka:DEFAULT_URL, consumerConfigs);

service kafka:Service on kafkaListener {
    remote function onConsumerRecord(kafka:Caller caller,kafka:ConsumerRecord[] records) returns error? {
        foreach var kafkaRecord in records {
            check processKafkaRecord(kafkaRecord);
        }
        kafka:Error? commitResult = caller->commit();
        if commitResult is error {
            log:printError("Error occurred while committing the " +
                "offsets for the consumer ", 'error = commitResult);
        }
    }
}

function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) returns error? {
    byte[] value = kafkaRecord.value;
    string messageContent = check string:fromBytes(value);
    log:printInfo("Received Message: " + messageContent);
}

type Student record {
    string Name;
    int StudentNo;
};
type Course record {
    int lecturerID;
    string lecturerName;
    int courseID ;
    
};

Student [] studentList = [];
Course [] courseList = [];

listener http:Listener securedEP = new(9090,
    secureSocket = {
        key: {
            certFile: "../resource/path/to/public.crt",
            keyFile: "../resource/path/to/private.key"
        }
    }
);


@http:ServiceConfig {
    auth: [
        {
            fileUserStoreConfig: {},
            scopes: ["admin"]
        }
    ]
}
service /hod on securedEP {
     resource function post addStudent(@http:Payload Student new_student) returns json {
        io:println("Adding new student to list..........");
        studentList.push(new_student);
        return ("Successfully added "+ new_student.StudentNo.toString() + " To the studentlist ");

        }
     resource function post addCourse(@http:Payload Course new_course) returns json {
        io:println("Adding new course to list..........");
        courseList.push(new_course);
        return ("Successfully added "+ new_course.courseID.toString()+ " To the courselist ");

        }
    }

 

