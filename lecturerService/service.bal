import ballerina/http;
import ballerina/io;
import ballerinax/kafka; 
import ballerina/log;

kafka:ConsumerConfiguration consumerConfigs = {
    groupId: "test-consumer-group",
    topics: ["dsar"],
    pollingInterval: 1,
    autoCommit: false

};
kafka:Consumer kafkaConsumer=check new(kafka:DEFAULT_URL,consumerConfigs);
kafka:TopicPartition topicPartition = {
    topic:"dsar",
    partition: 1

};
listener kafka:Listener kafkaListener =
        new (kafka:DEFAULT_URL, consumerConfigs);

service kafka:Service on kafkaListener {
    remote function onConsumerRecord(kafka:Caller caller,kafka:ConsumerRecord[] records) returns error? {
        foreach var kafkaRecord in records {
            check processKafkaRecord(kafkaRecord);
        }
        kafka:Error? commitResult = caller->commit();
        if commitResult is error {
            log:printError("Error occurred while committing the " +
                "offsets for the consumer ", 'error = commitResult);
        }
    }
}

function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) returns error? {
    byte[] value = kafkaRecord.value;
    string messageContent = check string:fromBytes(value);
    log:printInfo("Received Message: " + messageContent);
}


type CourseOutline record {
    string courseName;
    string learning_outcomes;
    string learningOutcomes; 
    string courseContent;
    string assessmentSchedule;
};
CourseOutline [] courseList = [];
listener http:Listener securedEP = new(9090,
    secureSocket = {
        key: {
            certFile: "../resource/path/to/public.crt",
            keyFile: "../resource/path/to/private.key"
        }
    }
);
@http:ServiceConfig {
    auth: [
        {
            fileUserStoreConfig: {},
            scopes: ["lecturer"]
        }
    ]
}
service /lecturer on securedEP {
     resource function post addCoursecontent(@http:Payload CourseOutline new_content) returns json {
        io:println("Adding content to list..........");
        courseList.push(new_content);
        return ("Successfully added "+ new_content.courseName+ " To the courselist ");
        }
    resource  function  get courseOutlines() returns  CourseOutline[] {
        io:println("Producing courseOutlines to client ............");
          return courseList;
        }
}